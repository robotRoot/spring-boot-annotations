package com.AM.Service;

import java.util.List;
import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.AM.Entity.Customer;

/**
 * @author Acep Munawar 
 *  by bushansirgur.in
 * 
 * */
@Service
public class CustomerService {

	private static List list = new ArrayList<>();
	static {
		Customer customer = new Customer();
		customer.setId(1L);
		customer.setName("Customer 1");
		customer.setAge(26L);
		customer.setLocation("Jl Test");
		list.add(customer);
		
		customer = new Customer();
		customer.setId(2L);
		customer.setName("Acep Munawar");
		customer.setAge(27L);
		customer.setLocation("Jl Kemana saja");
		list.add(customer);

		customer = new Customer();
		customer.setId(3l);
		customer.setName("Danil Martono");
		customer.setAge(28L);
		customer.setLocation("Jl Damai");
		list.add(customer);
	}
	public List getCustomerList(){
		return list;
	}
}
