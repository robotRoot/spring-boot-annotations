package com.AM;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Acep Munawar 
 *  by bushansirgur.in
 * 
 * */

@SpringBootApplication
public class SpringBootBasicAnnotationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootBasicAnnotationApplication.class, args);
	}

}
