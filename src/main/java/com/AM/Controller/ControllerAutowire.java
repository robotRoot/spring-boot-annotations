package com.AM.Controller;

import com.AM.Service.CustomerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Acep Munawar 
 *  by bushansirgur.in
 * 
 * */
@RestController
public class ControllerAutowire {

    @Autowired
    CustomerService customerService;

    @GetMapping("/customersAutowired")
    public List getList(){
        return customerService.getCustomerList();
    }
}
