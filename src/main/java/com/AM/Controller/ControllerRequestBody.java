package com.AM.Controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.AM.model.User;

/**
 * @author Acep Munawar 
 *  by bushansirgur.in
 * 
 * */
@RestController
public class ControllerRequestBody {
	
	@PostMapping("/users")
	public void printData(@RequestBody User user) {
		System.out.println(user);
	}
	
}
