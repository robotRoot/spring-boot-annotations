package com.AM.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Acep Munawar 
 *  by bushansirgur.in
 * 
 * */
@RestController
public class ControllerRequestParam {
	
	@GetMapping("/users")
	public String getName(@RequestParam(name = "userName") String name) {
		return "My Name Is " + name;
	}
	/**
	 * desc
	 * 
	 * get path localhost:8080/users?userName=acepmunawar 
	 * 
	 * */	
	
	@GetMapping("/customers")
	public String getCustomerName(@RequestParam(defaultValue = "anonymous") String name) {
		return "My Name Is" + name;
	}
	/**
	 * desc
	 * 
	 * get path localhost:8080/customers
	 * 
	 * 
	 * 
	 * */	
	
	@GetMapping("/employess")
	public String getEmployeeName(@RequestParam(required = false) String name) {
		return "My Name is " + name;
	}
	/**
	 * desc
	 * 
	 * get path localhost:8080/employees
	 * 
	 * */	
	
}
