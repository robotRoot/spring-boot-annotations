package com.AM.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.AM.model.User;

/**
 * @author Acep Munawar 
 *  by bushansirgur.in
 * 
 * */
@Controller
public class ControllerResponseBody {
	
	@GetMapping("/usersBody")
	public @ResponseBody User getUser() {
		User user = new User();
			user.setFirstName("Acep");
			user.setLastName("Munawar");
			user.setAge(27);
		return user;
	}
	
}
