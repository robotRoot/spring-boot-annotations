package com.AM.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

//Description
/**
 * path variable adalah sebuah variable yang dapat di gunakan 
 * pada path url. fungsinya sama dengan variable pada umumnya 
 * hanya path varible
 * 
 * */



/**
 * @author Acep Munawar 
 *  by bushansirgur.in
 * 
 * */

@RestController
public class ControllerPathVariable {

	@GetMapping("/sayhello")
	public String SayHello() {
		return "say hello";
	}
	
	@GetMapping("/user/{userName}")
	public String UserName(@PathVariable("userName") String username) {
		return "Username : " + username;
	}
	
	@GetMapping("/user/password/{pass}")
	public String Password(@PathVariable("password") String pass) {
		return "Password : " + pass +"n";
	}
	
	@GetMapping("/user/{Name}/{Email}")
	public String View(@PathVariable("Name") String name, @PathVariable("Email") String email) {
		return "Nama : " + name + "\n" + "Email : " + email;
	}
}
