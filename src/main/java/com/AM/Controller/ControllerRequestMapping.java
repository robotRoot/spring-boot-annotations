package com.AM.Controller;

import java.lang.reflect.Method;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Acep Munawar 
 *  by bushansirgur.in
 * 
 * */
@RestController
@RequestMapping("/users")
public class ControllerRequestMapping {

	@RequestMapping("/user")
	public String getUser() {
		return "get user";
	}

	/**
	 * desc
	 * 
	 * get path : http://localhost:8080/users/user
	 * 
	 */

	@RequestMapping(value = { "/seconduser", "/second-user", "/secondUser" }, method = RequestMethod.GET)
	public String getSecondUser() {
		return "get second user";
	}

}
